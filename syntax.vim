"if exists("b:current_syntax")
"    finish
"endif

"let b:current_syntax = "atom"

syn keyword atomKeywords    new mut pub fn class typedef extern return if elif else let
syn keyword atomTypes       ptr byte int int64 float float64 bool str void Array
syn keyword atomBool        true false
syn match   atomArrow       display "->"
syn match   atomNum         display "\d\('\=\d\+\)*\(u\=l\{0,2}\|ll\=u\)\>"
syn match   atomOp          display "\%(+\|-\|/\|*\|=\|\^\|&\||\|!\|>\|<\|%\)=\?"
syn match   atomFnCall      "\w\(\w\)*("he=e-1,me=e-1

hi def link atomTypes       Type
hi def link atomBool        Boolean
hi def link atomNum         Number
hi def link atomArrow       Operator
hi def link atomOp          Operator
hi def link atomFnCall      Function
hi def link atomKeywords    Statement
