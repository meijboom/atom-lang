use std::iter::Iterator;

pub struct MultipeekIter<I: Iterator> {
    iter: I,
    is_eof: bool,
    peeked: Vec<I::Item>,
}

impl<I: Iterator> MultipeekIter<I> {
    pub fn new(iter: I) -> MultipeekIter<I> {
        MultipeekIter {
            iter,
            is_eof: false,
            peeked: vec![],
        }
    }

    pub fn peek(&mut self) -> Option<&I::Item> {
        self.peek_nth(0)
    }

    pub fn peek_nth(&mut self, n: usize) -> Option<&I::Item> {
        if self.peeked.len() < n + 1 {
            for _ in 0..(n + 1) - self.peeked.len() {
                match self.iter.next() {
                    Some(item) => self.peeked.push(item),
                    None => {
                        self.is_eof = true;
                        return None;
                    }
                }
            }
        }

        self.peeked.get(n)
    }
}

impl<I: Iterator> Iterator for MultipeekIter<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        match self.peeked.first() {
            Some(_) => Some(self.peeked.remove(0)),
            None => self.iter.next(),
        }
    }

    fn count(self) -> usize {
        if self.is_eof {
            return self.peeked.len();
        }

        self.peeked.len() + self.iter.count()
    }

    fn nth(&mut self, n: usize) -> Option<I::Item> {
        let drained = self.peeked.drain(..n);

        if drained.len() < n + 1 {
            if self.is_eof {
                return None;
            }

            return self.iter.nth(n - drained.len());
        }

        drained.last()
    }

    fn last(mut self) -> Option<I::Item> {
        self.iter
            .last()
            .or(Some(self.peeked.remove(self.peeked.len() - 1)))
    }
}
