use std::error;
use std::fmt;
use std::result;

#[derive(Debug)]
pub struct ParseError {
    pub pos: (usize, usize),
    pub message: String,
    pub is_eof: bool,
}

impl error::Error for ParseError {}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "ParseError: {} at {}:{}",
            self.message, self.pos.0, self.pos.1,
        )
    }
}

impl ParseError {
    pub fn eof() -> Self {
        Self {
            pos: (0, 0),
            message: "EOF".to_string(),
            is_eof: true,
        }
    }

    pub fn new<T: ToString>(message: T, pos: (usize, usize)) -> Self {
        Self {
            pos,
            message: message.to_string(),
            is_eof: false,
        }
    }
}

pub type Result<T> = result::Result<T, ParseError>;
