use logos::{Lexer, Span, SpannedIter};

use super::ast::*;
use super::lexer::Token;
use super::multipeek_iter::MultipeekIter;
use super::result::{ParseError, Result};

const MAX_PRIORITY: usize = 100;

pub struct Parser<'a> {
    indent_lvl: usize,
    line_number: usize,
    column: usize,
    tokens: MultipeekIter<SpannedIter<'a, Token>>,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: Lexer<'a, Token>) -> Self {
        Self {
            column: 1,
            indent_lvl: 0,
            line_number: 1,
            tokens: MultipeekIter::new(lexer.spanned()),
        }
    }

    fn next(&mut self) -> Result<(Token, Span)> {
        if let Some((token, pos)) = self.tokens.next() {
            if token.eq(&Token::Newline) {
                self.line_number += 1;
                self.column = 1;
            }

            return Ok((token, pos));
        }

        Err(ParseError::eof())
    }

    fn peek(&mut self) -> Result<&(Token, Span)> {
        if let Some(token) = self.tokens.peek() {
            return Ok(token);
        }

        Err(ParseError::eof())
    }

    fn pos(&self) -> Pos {
        (self.line_number, self.column)
    }

    fn try_peek(&mut self) -> Result<Option<&(Token, Span)>> {
        if let Some(token) = self.tokens.peek() {
            return Ok(Some(token));
        }

        Ok(None)
    }

    fn accept(&mut self, expected: &Token) -> Result<bool> {
        if let Some((token, _)) = self.try_peek()? {
            return Ok(token.eq(expected));
        }

        Ok(false)
    }

    fn expect(&mut self, expected: &Token) -> Result<(Token, Span)> {
        let (token, span) = self.next()?;

        self.column += span.end - span.start;

        if !token.eq(expected) {
            return Err(ParseError::new(
                format!("expected {:?} got {:?}", token, expected),
                (self.line_number, self.column),
            ));
        }

        Ok((token, span))
    }

    fn expect_indent(&mut self) -> Result<usize> {
        if let (Token::Indent(size), _) = self.expect(&Token::Indent(0))? {
            return Ok(size);
        }

        panic!("invalid token")
    }

    fn expect_ident(&mut self) -> Result<String> {
        if let (Token::Ident(name), _) = self.expect(&Token::Ident(String::new()))? {
            return Ok(name);
        }

        panic!("invalid token")
    }

    fn expect_int(&mut self) -> Result<i64> {
        if let (Token::Int(name), _) = self.expect(&Token::Int(0))? {
            return Ok(name);
        }

        panic!("invalid token")
    }

    fn parse_items<T, F>(&mut self, end_token: &Token, callback: F) -> Result<Vec<T>>
    where
        F: Fn(&mut Self) -> Result<T>,
    {
        let (token, _) = self.peek()?;

        if token == end_token {
            return Ok(vec![]);
        }

        let mut items = vec![];

        loop {
            items.push(callback(self)?);

            let (token, _) = self.peek()?;

            if token == end_token {
                break;
            }

            self.expect(&Token::Separator)?;
        }

        Ok(items)
    }

    fn parse_surrounded_items<T, F>(
        &mut self,
        start_token: &Token,
        end_token: &Token,
        callback: F,
    ) -> Result<Vec<T>>
    where
        F: Fn(&mut Self) -> Result<T>,
    {
        self.expect(&start_token)?;
        let items = self.parse_items(end_token, callback)?;
        self.expect(&end_token)?;

        Ok(items)
    }

    fn parse_optional_surrounded_items<T, F>(
        &mut self,
        start_token: &Token,
        end_token: &Token,
        callback: F,
    ) -> Result<Vec<T>>
    where
        F: Fn(&mut Self) -> Result<T>,
    {
        if self.accept(start_token)? {
            return self.parse_surrounded_items(start_token, end_token, callback);
        }

        Ok(vec![])
    }

    fn skip_newlines(&mut self) -> Result<bool> {
        let mut skipped = false;

        // @TODO: maybe skip newlines in the lexer we don't use in the parser?
        loop {
            if self.accept(&Token::Newline)? {
                self.expect(&Token::Newline)?;
                skipped = true;
                continue;
            }

            break;
        }

        Ok(skipped)
    }

    fn parse_type(&mut self) -> Result<Type> {
        let name = self.expect_ident()?;
        let generic_args =
            self.parse_optional_surrounded_items(&Token::CaretLeft, &Token::CaretRight, |p| {
                p.parse_type()
            })?;

        Ok(Type { name, generic_args })
    }

    // parse only value expressions (scalars, idents etc.)
    fn parse_value_expr(&mut self) -> Result<Node<Expr>> {
        let pos_start = self.pos();
        let (token, _) = self.peek()?;

        match token {
            Token::New => {
                self.expect(&Token::New)?;
                let typedef = self.parse_type()?;

                let props =
                    self.parse_surrounded_items(&Token::BracketLeft, &Token::BracketRight, |p| {
                        p.parse_init_prop()
                    })?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Init((typedef, props)),
                ))
            }
            Token::SqrBracketLeft => {
                let items = self.parse_surrounded_items(
                    &Token::SqrBracketLeft,
                    &Token::SqrBracketRight,
                    |p| p.parse_expr(MAX_PRIORITY),
                )?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Scalar(Scalar::Array(items)),
                ))
            }
            Token::ParentLeft => {
                let items =
                    self.parse_surrounded_items(&Token::ParentLeft, &Token::ParentRight, |p| {
                        p.parse_expr(MAX_PRIORITY)
                    })?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Scalar(Scalar::Tuple(items)),
                ))
            }
            Token::Int(val) => {
                let val = *val;
                self.next()?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Scalar(if val <= i32::MAX as i64 {
                        Scalar::Int(val as i32)
                    } else {
                        Scalar::Int64(val)
                    }),
                ))
            }
            Token::Float(val) => {
                let val = *val;
                self.next()?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Scalar(if val <= f32::MAX as f64 {
                        Scalar::Float(val as f32)
                    } else {
                        Scalar::Float64(val)
                    }),
                ))
            }
            Token::Bool(val) => {
                let val = *val;
                self.next()?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Scalar(Scalar::Bool(val)),
                ))
            }
            Token::String(val) => {
                let val = val.clone();
                self.next()?;

                Ok(new_expr(
                    pos_start,
                    self.pos(),
                    Expr::Scalar(Scalar::Str(val)),
                ))
            }
            Token::Ident(_) => Ok(new_expr(
                pos_start,
                self.pos(),
                Expr::Ident(self.expect_ident()?),
            )),
            token => Err(ParseError::new(
                format!("unexpected {:?}", token),
                (self.line_number, self.column),
            )),
        }
    }

    fn parse_expr(&mut self, max_priority: usize) -> Result<Node<Expr>> {
        let mut priority = 1;
        let mut expr = self.parse_value_expr()?;

        loop {
            let pos_start = self.pos();
            let (token, _) = self.peek()?;

            match priority {
                1 if priority <= max_priority => match token {
                    Token::ParentLeft => {
                        let args = self.parse_surrounded_items(
                            &Token::ParentLeft,
                            &Token::ParentRight,
                            |p| p.parse_expr(MAX_PRIORITY),
                        )?;

                        expr = new_expr(
                            expr.pos_start,
                            self.pos(),
                            Expr::FnCall((expr.into(), args)),
                        );
                    }
                    Token::SqrBracketLeft => {
                        self.expect(&Token::SqrBracketLeft)?;
                        let index = self.expect_int()?;
                        self.expect(&Token::SqrBracketRight)?;

                        expr = new_expr(
                            expr.pos_start,
                            self.pos(),
                            Expr::Index((expr.into(), index as usize)),
                        );
                    }
                    Token::Dot => {
                        self.expect(&Token::Dot)?;

                        let pos_start = self.pos();
                        let name = self.expect_ident()?;

                        expr = new_expr(
                            expr.pos_start,
                            self.pos(),
                            Expr::Member((
                                expr.into(),
                                Node::<Ident> {
                                    kind: Ident { name },
                                    pos_start,
                                    pos_end: self.pos(),
                                },
                            )),
                        );
                    }
                    _ => {
                        priority += 1;
                    }
                },

                2 if priority <= max_priority => match token {
                    Token::Multiplication => {
                        self.expect(&Token::Multiplication)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::ArithmeticOp((
                                ArithmeticOp::Mul,
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    Token::Division => {
                        self.expect(&Token::Division)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::ArithmeticOp((
                                ArithmeticOp::Div,
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    _ => {
                        priority += 1;
                    }
                },

                3 if priority <= max_priority => match token {
                    Token::Addition => {
                        self.expect(&Token::Addition)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::ArithmeticOp((
                                ArithmeticOp::Add,
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    Token::Subtraction => {
                        self.expect(&Token::Subtraction)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::ArithmeticOp((
                                ArithmeticOp::Sub,
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    _ => {
                        priority += 1;
                    }
                },

                4 if priority <= max_priority => match token {
                    Token::CaretLeft => {
                        self.expect(&Token::CaretLeft)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::BinaryOp((
                                if self.accept(&Token::Equals)? {
                                    BinaryOp::Lte
                                } else {
                                    BinaryOp::Lt
                                },
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    Token::CaretRight => {
                        self.expect(&Token::CaretRight)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::BinaryOp((
                                if self.accept(&Token::Equals)? {
                                    BinaryOp::Gte
                                } else {
                                    BinaryOp::Gt
                                },
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    _ => {
                        priority += 1;
                    }
                },

                5 if priority <= max_priority => match token {
                    Token::And => {
                        self.expect(&Token::And)?;

                        let right = self.parse_expr(priority - 1)?;

                        expr = new_expr(
                            expr.pos_start,
                            self.pos(),
                            Expr::LogicalOp((LogicalOp::And, expr.into(), right.into())),
                        );
                    }
                    Token::Or => {
                        self.expect(&Token::Or)?;

                        let right = self.parse_expr(priority - 1)?;

                        expr = new_expr(
                            expr.pos_start,
                            self.pos(),
                            Expr::LogicalOp((LogicalOp::Or, Box::new(expr), Box::new(right))),
                        );
                    }
                    _ => {
                        priority += 1;
                    }
                },

                6 if priority <= max_priority => match token {
                    Token::Not => {
                        self.expect(&Token::Not)?;
                        self.expect(&Token::Equals)?;

                        expr = new_expr(
                            pos_start,
                            self.pos(),
                            Expr::BinaryOp((
                                BinaryOp::Neq,
                                expr.into(),
                                self.parse_expr(priority - 1)?.into(),
                            )),
                        );
                    }
                    Token::Equals => {
                        if let Some((Token::Equals, _)) = self.tokens.peek_nth(1) {
                            self.expect(&Token::Equals)?;
                            self.expect(&Token::Equals)?;

                            expr = new_expr(
                                pos_start,
                                self.pos(),
                                Expr::BinaryOp((
                                    BinaryOp::Eq,
                                    expr.into(),
                                    self.parse_expr(priority - 1)?.into(),
                                )),
                            );
                        } else {
                            break;
                        }
                    }
                    _ => {
                        priority += 1;
                    }
                },

                _ => break,
            }
        }

        Ok(expr)
    }

    fn parse_fn_arg(&mut self) -> Result<FnArg> {
        let name = self.expect_ident()?;
        self.expect(&Token::Colon)?;

        Ok(FnArg {
            name,
            typedef: self.parse_type()?,
        })
    }

    fn parse_init_prop(&mut self) -> Result<InitProperty> {
        let name = self.expect_ident()?;
        self.expect(&Token::Equals)?;

        Ok(InitProperty {
            name,
            expr: self.parse_expr(MAX_PRIORITY)?,
        })
    }

    fn parse_property(&mut self) -> Result<Property> {
        let is_public = if self.accept(&Token::Public)? {
            self.expect(&Token::Public)?;
            true
        } else {
            false
        };
        let name = self.expect_ident()?;
        self.expect(&Token::Colon)?;

        Ok(Property {
            name,
            is_public,
            typedef: self.parse_type()?,
        })
    }

    fn parse_stmt(&mut self) -> Result<Node<Stmt>> {
        // first, check proper indenting
        if self.indent_lvl > 0 {
            let level = self.expect_indent()?;

            if level != self.indent_lvl {
                return Err(ParseError::new(
                    format!(
                        "expecting indent lvl {} but found {}",
                        self.indent_lvl, level
                    ),
                    (self.line_number, self.column),
                ));
            }
        }

        // then, parse the stmt
        let pos_start = self.pos();
        let (token, _) = self.peek()?;

        match token {
            Token::External | Token::Function => {
                let is_external = if self.accept(&Token::External)? {
                    self.expect(&Token::External)?;
                    true
                } else {
                    false
                };

                let fn_def = self.parse_fn_def(!is_external)?;

                Ok(new_stmt(
                    pos_start,
                    self.pos(),
                    if is_external {
                        Stmt::ExternFnDef(fn_def)
                    } else {
                        Stmt::FnDef(fn_def)
                    },
                ))
            }
            Token::Let => {
                self.expect(&Token::Let)?;

                let mutable = if self.accept(&Token::Mutable)? {
                    self.expect(&Token::Mutable)?;
                    true
                } else {
                    false
                };

                let key = self.parse_expr(2)?;
                self.expect(&Token::Equals)?;
                let value = self.parse_expr(MAX_PRIORITY)?;
                self.expect(&Token::Newline)?;

                Ok(new_stmt(
                    value.pos_start,
                    self.pos(),
                    Stmt::Assign(Assign {
                        key,
                        value,
                        reassign: false,
                        mutable,
                    }),
                ))
            }
            Token::Return => {
                self.expect(&Token::Return)?;

                let expr = self.parse_expr(MAX_PRIORITY)?;
                self.expect(&Token::Newline)?;

                Ok(new_stmt(pos_start, self.pos(), Stmt::Return(expr)))
            }
            Token::If => {
                self.expect(&Token::If)?;

                let if_else = self.parse_if_stmt(false)?;

                Ok(new_stmt(pos_start, self.pos(), Stmt::If(if_else)))
            }
            Token::Typedef => {
                self.expect(&Token::Typedef)?;
                let name = self.expect_ident()?;
                self.expect(&Token::Colon)?;

                let typedef = self.parse_type()?;

                Ok(new_stmt(
                    pos_start,
                    self.pos(),
                    Stmt::Typedef((name, typedef)),
                ))
            }
            Token::Class => {
                self.expect(&Token::Class)?;
                let name = self.expect_ident()?;
                let generic_args = self.parse_optional_surrounded_items(
                    &Token::CaretLeft,
                    &Token::CaretRight,
                    |p| p.parse_type(),
                )?;

                self.expect(&Token::Newline)?;

                let mut props = vec![];

                self.indent_lvl += 1;

                loop {
                    match self.try_peek()? {
                        Some(&(Token::Indent(level), _)) => {
                            if level != self.indent_lvl {
                                break;
                            }

                            self.expect_indent()?;
                            props.push(self.parse_property()?);
                            self.expect(&Token::Newline)?;
                        }

                        Some(&(Token::Newline, _)) => {
                            self.expect(&Token::Newline)?;

                            break;
                        }
                        _ => break,
                    };
                }

                let mut methods = vec![];

                loop {
                    match self.try_peek()? {
                        Some(&(Token::Indent(level), _)) => {
                            if level != self.indent_lvl {
                                break;
                            }

                            self.expect_indent()?;
                            methods.push(self.parse_fn_def(true)?);

                            if let Some(&(Token::Newline, _)) = self.try_peek()? {
                                self.expect(&Token::Newline)?;
                            }
                        }
                        _ => break,
                    };
                }

                self.indent_lvl -= 1;

                Ok(new_stmt(
                    pos_start,
                    self.pos(),
                    Stmt::ClassDef(ClassDef {
                        name,
                        props,
                        methods,
                        generic_args,
                    }),
                ))
            }
            _ => {
                let expr = self.parse_expr(MAX_PRIORITY)?;

                if self.accept(&Token::Equals)? {
                    self.expect(&Token::Equals)?;
                    let value = self.parse_expr(MAX_PRIORITY)?;
                    self.expect(&Token::Newline)?;

                    return Ok(new_stmt(
                        expr.pos_start,
                        value.pos_end,
                        Stmt::Assign(Assign {
                            key: expr,
                            value,
                            mutable: false,
                            reassign: true,
                        }),
                    ));
                }

                self.expect(&Token::Newline)?;

                Ok(new_stmt(pos_start, self.pos(), Stmt::Expr(expr)))
            }
        }
    }

    fn parse_fn_def(&mut self, has_body: bool) -> Result<FnDef> {
        self.expect(&Token::Function)?;

        let name = self.expect_ident()?;
        let generic_args =
            self.parse_optional_surrounded_items(&Token::CaretLeft, &Token::CaretRight, |p| {
                p.parse_type()
            })?;

        let args =
            self.parse_optional_surrounded_items(&Token::ParentLeft, &Token::ParentRight, |p| {
                p.parse_fn_arg()
            })?;

        let return_type = if self.accept(&Token::Newline)? {
            Type {
                name: "void".to_string(),
                generic_args: vec![],
            }
        } else {
            self.expect(&Token::Arrow)?;
            self.parse_type()?
        };

        let body = if has_body {
            self.expect(&Token::Newline)?;

            self.indent_lvl += 1;
            let body = self.parse_block()?;
            self.indent_lvl -= 1;

            body
        } else {
            vec![]
        };

        Ok(FnDef {
            name,
            args,
            body,
            return_type,
            generic_args,
        })
    }

    fn parse_if_stmt(&mut self, inner: bool) -> Result<IfElse> {
        let cond = self.parse_expr(MAX_PRIORITY)?;
        self.expect(&Token::Newline)?;

        self.indent_lvl += 1;
        let body = self.parse_block()?;
        self.indent_lvl -= 1;

        if body.is_empty() {
            return Err(ParseError::new(
                "expecting body in if-statement",
                (self.line_number, self.column),
            ));
        }

        let mut alt = vec![];
        let mut or = None;

        if let Some((Token::Indent(level), _)) = self.try_peek()? {
            if *level == self.indent_lvl {
                if let Some((Token::ElseIf, _)) = self.tokens.peek_nth(1) {
                    self.expect_indent()?;
                    self.expect(&Token::ElseIf)?;

                    or = Some(Box::new(self.parse_if_stmt(true)?));
                }

                if let Some((Token::Else, _)) = self.tokens.peek_nth(1) {
                    if !inner {
                        self.expect_indent()?;
                        self.expect(&Token::Else)?;
                        self.expect(&Token::Newline)?;

                        self.indent_lvl += 1;
                        alt = self.parse_block()?;
                        self.indent_lvl -= 1;
                    }
                }
            }
        }

        Ok(IfElse {
            cond,
            body,
            or,
            alt,
        })
    }

    fn parse_block(&mut self) -> Result<Vec<Node<Stmt>>> {
        let mut block = vec![];

        loop {
            let stmt = self.parse_stmt();

            if let Ok(stmt) = stmt {
                block.push(stmt);

                let result = self.try_peek()?;

                if result.is_none() {
                    break;
                }

                self.skip_newlines()?;

                if let Some(&(Token::Indent(level), _)) = self.try_peek()? {
                    if self.indent_lvl > 0 && level == self.indent_lvl - 1 {
                        break;
                    }
                } else if self.indent_lvl > 0 {
                    break;
                }

                continue;
            } else if let Err(e) = stmt {
                if e.is_eof {
                    break;
                }

                return Err(e);
            }
        }

        Ok(block)
    }

    pub fn parse(mut self) -> Result<Tree> {
        self.parse_block()
    }
}
