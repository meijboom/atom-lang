use serde::Serialize;

use std::fmt;

#[derive(Debug, Serialize, Clone)]
pub struct Type {
    pub name: String,
    pub generic_args: Vec<Type>,
}

#[derive(Debug, Serialize, Clone)]
pub struct FnArg {
    pub name: String,
    pub typedef: Type,
}

#[derive(Debug, Serialize, Clone)]
pub struct FnDef {
    pub body: Tree,
    pub name: String,
    pub args: Vec<FnArg>,
    pub return_type: Type,
    pub generic_args: Vec<Type>,
}

#[derive(Debug, Serialize, Clone)]
pub struct IfElse {
    pub cond: Node<Expr>,
    pub body: Vec<Node<Stmt>>,
    pub or: Option<Box<IfElse>>,
    pub alt: Vec<Node<Stmt>>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Assign {
    pub key: Node<Expr>,
    pub value: Node<Expr>,
    pub reassign: bool,
    pub mutable: bool,
}

#[derive(Debug, Serialize, Clone)]
pub enum Scalar {
    Int(i32),
    Int64(i64),
    Str(String),
    Float(f32),
    Float64(f64),
    Bool(bool),
    Tuple(Vec<Node<Expr>>),
    Array(Vec<Node<Expr>>),
}

#[derive(Debug, Serialize, Clone)]
pub struct Ident {
    pub name: String,
}

#[derive(Debug, Serialize, PartialEq, Clone)]
pub enum BinaryOp {
    Lt,
    Lte,
    Gt,
    Gte,
    Eq,
    Neq,
}

#[derive(Debug, Serialize, Clone)]
pub enum LogicalOp {
    And,
    Or,
}

#[derive(Debug, Serialize, Clone)]
pub enum ArithmeticOp {
    Add,
    Sub,
    Mul,
    Div,
}

impl ArithmeticOp {
    pub fn name(&self) -> &str {
        match self {
            Self::Add => "add",
            Self::Sub => "sub",
            Self::Mul => "mul",
            Self::Div => "div",
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub enum Expr {
    Ident(String),
    Scalar(Scalar),
    Index((Box<Node<Expr>>, usize)),
    Member((Box<Node<Expr>>, Node<Ident>)),
    Init((Type, Vec<InitProperty>)),
    FnCall((Box<Node<Expr>>, Vec<Node<Expr>>)),
    BinaryOp((BinaryOp, Box<Node<Expr>>, Box<Node<Expr>>)),
    LogicalOp((LogicalOp, Box<Node<Expr>>, Box<Node<Expr>>)),
    ArithmeticOp((ArithmeticOp, Box<Node<Expr>>, Box<Node<Expr>>)),
}

impl Expr {
    pub fn name(&self) -> &str {
        match self {
            Self::Index(_) => "index",
            Self::Scalar(_) => "scalar",
            Self::Init(_) => "initializer",
            Self::Ident(_) => "identifier",
            Self::FnCall(_) => "function call",
            Self::Member(_) => "member expression",
            Self::BinaryOp(_) => "binary operation",
            Self::LogicalOp(_) => "logical operation",
            Self::ArithmeticOp(_) => "arithmetic operation",
        }
    }
}

pub type Path = Vec<String>;

#[derive(Debug, Serialize, Clone)]
pub struct Import {
    pub name: Path,
}

impl fmt::Display for Import {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name.join("."))
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct InitProperty {
    pub name: String,
    pub expr: Node<Expr>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Property {
    pub name: String,
    pub typedef: Type,
    pub is_public: bool,
}

#[derive(Debug, Serialize, Clone)]
pub struct ClassDef {
    pub name: String,
    pub props: Vec<Property>,
    pub methods: Vec<FnDef>,
    pub generic_args: Vec<Type>,
}

#[derive(Debug, Serialize, Clone)]
pub enum Stmt {
    If(IfElse),
    FnDef(FnDef),
    Import(Import),
    ExternFnDef(FnDef),
    Typedef((String, Type)),
    ClassDef(ClassDef),
    Expr(Node<Expr>),
    Return(Node<Expr>),
    Assign(Assign),
}

impl Stmt {
    pub fn name(&self) -> &str {
        match self {
            Self::If(_) => "if",
            Self::Return(_) => "return",
            Self::Import(_) => "import",
            Self::Expr(_) => "expression",
            Self::Assign(_) => "assignment",
            Self::Typedef(_) => "type definition",
            Self::ExternFnDef(_) => "external function definition",
            Self::ClassDef(_) => "class definition",
            Self::FnDef(_) => "function definition",
        }
    }
}

pub type Pos = (usize, usize);

#[derive(Debug, Serialize, Clone)]
pub struct Node<T> {
    pub pos_start: Pos,
    pub pos_end: Pos,
    pub kind: T,
}

pub fn new_expr(start: Pos, end: Pos, expr: Expr) -> Node<Expr> {
    Node::<Expr> {
        pos_start: start,
        pos_end: end,
        kind: expr,
    }
}

pub fn new_stmt(start: Pos, end: Pos, stmt: Stmt) -> Node<Stmt> {
    Node::<Stmt> {
        pos_start: start,
        pos_end: end,
        kind: stmt,
    }
}

pub type Tree = Vec<Node<Stmt>>;
