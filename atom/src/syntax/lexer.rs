use std::mem;

use logos::Logos;

#[derive(Logos, Debug, PartialEq)]
pub enum Token {
    #[token("fn")]
    Function,

    #[token("pub")]
    Public,

    #[token("new")]
    New,

    #[token("mut")]
    Mutable,

    #[token("extern")]
    External,

    #[token("class")]
    Class,

    #[token("typedef")]
    Typedef,

    #[token("let")]
    Let,

    #[token("if")]
    If,

    #[token("and")]
    And,

    #[token("or")]
    Or,

    #[token("elif")]
    ElseIf,

    #[token("else")]
    Else,

    #[token("return")]
    Return,

    #[token("->")]
    Arrow,

    #[regex(r"([ ][ ][ ][ ])+", |lex| lex.slice().matches("    ").count())]
    Indent(usize),

    #[token(",")]
    Separator,

    #[token(".")]
    Dot,

    #[token(":")]
    Colon,

    #[token("=")]
    Equals,

    #[token("!")]
    Not,

    #[token("-")]
    Subtraction,

    #[token("+")]
    Addition,

    #[token("*")]
    Multiplication,

    #[token("/")]
    Division,

    #[token("(")]
    ParentLeft,

    #[token(")")]
    ParentRight,

    #[token("<")]
    CaretLeft,

    #[token(">")]
    CaretRight,

    #[token("{")]
    BracketLeft,

    #[token("}")]
    BracketRight,

    #[token("[")]
    SqrBracketLeft,

    #[token("]")]
    SqrBracketRight,

    #[regex("\"([^\"\\\\]|\\\\.)*\"", |lex| lex.slice().parse())]
    String(String),

    #[regex(r"[0-9]+", |lex| lex.slice().parse())]
    Int(i64),

    #[regex(r"-?[0-9]+\.[0-9]+", |lex| lex.slice().parse())]
    Float(f64),

    #[regex(r"(true|false)", |lex| lex.slice().parse())]
    Bool(bool),

    #[regex(r"[a-zA-Z][a-zA-Z_0-9]*", |lex| lex.slice().parse())]
    Ident(String),

    #[token("\n")]
    Newline,

    #[error]
    #[regex(r"[ \f]+", logos::skip)]
    Error,
}

impl Token {
    pub fn eq(&self, other: &Token) -> bool {
        mem::discriminant(self) == mem::discriminant(other)
    }
}
