use std::error;
use std::fmt;
use std::result;

#[derive(Debug)]
pub struct CodegenError {
    pub message: String,
}

impl error::Error for CodegenError {}

impl fmt::Display for CodegenError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "CodegenError: {}", self.message,)
    }
}

impl CodegenError {
    pub fn new<T: ToString>(message: T) -> Self {
        Self {
            message: message.to_string(),
        }
    }
}

pub type Result<T> = result::Result<T, CodegenError>;
