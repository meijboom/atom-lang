use std::collections::HashMap;
use std::path::Path;

use super::result::{CodegenError, Result};

use inkwell::{
    builder::Builder,
    context::Context,
    module::{Linkage, Module as LlvmModule},
    targets::{CodeModel, FileType, InitializationConfig, RelocMode, Target, TargetMachine},
    types::{AnyTypeEnum, BasicTypeEnum, StructType},
    values::{BasicValueEnum, PointerValue},
    AddressSpace, FloatPredicate, IntPredicate, OptimizationLevel,
};

use crate::compiler::ir::{Block, Class, Const, Func, Module, Type, TypeKind, IR};

pub struct Codegen {
    module: Module,
}

impl Codegen {
    pub fn new(module: Module) -> Self {
        Self { module }
    }

    fn create_class<'c>(
        &self,
        module: &LlvmModule<'c>,
        context: &'c Context,
        class: &Class,
    ) -> Result<StructType<'c>> {
        let struct_name = format!("c{}", class.id);

        match module.get_struct_type(&struct_name) {
            Some(struct_type) => Ok(struct_type),
            None => {
                let mut prop_types = vec![];
                let struct_type = context.opaque_struct_type(&struct_name);

                for prop in class.props.iter() {
                    prop_types.push(self.convert_basic_type(module, context, &prop.typedef)?);
                }

                struct_type.set_body(prop_types.as_slice(), false);

                Ok(struct_type)
            }
        }
    }

    fn convert_basic_type<'c>(
        &self,
        module: &LlvmModule<'c>,
        context: &'c Context,
        typedef: &Type,
    ) -> Result<BasicTypeEnum<'c>> {
        Ok(match self.convert_type(module, context, typedef)? {
            AnyTypeEnum::ArrayType(result) => result.into(),
            AnyTypeEnum::IntType(result) => result.into(),
            AnyTypeEnum::FloatType(result) => result.into(),
            AnyTypeEnum::PointerType(result) => result.into(),
            AnyTypeEnum::StructType(result) => result.into(),
            _ => {
                return Err(CodegenError::new(format!(
                    "expected basic_type for {}",
                    typedef.name
                )))
            }
        })
    }

    fn convert_type<'c>(
        &self,
        module: &LlvmModule<'c>,
        context: &'c Context,
        typedef: &Type,
    ) -> Result<AnyTypeEnum<'c>> {
        Ok(match &typedef.kind {
            TypeKind::Unknown => {
                return Err(CodegenError::new(format!(
                    "unable to construct unknown type {}",
                    typedef.name
                )))
            }
            TypeKind::Void => AnyTypeEnum::VoidType(context.void_type()),
            TypeKind::Bool => AnyTypeEnum::IntType(context.bool_type()),
            TypeKind::Int8 => AnyTypeEnum::IntType(context.i8_type()),
            TypeKind::Int => AnyTypeEnum::IntType(context.i32_type()),
            TypeKind::Int64 => AnyTypeEnum::IntType(context.i64_type()),
            TypeKind::Float => AnyTypeEnum::FloatType(context.f32_type()),
            TypeKind::Float64 => AnyTypeEnum::FloatType(context.f64_type()),
            TypeKind::Class(id) => AnyTypeEnum::StructType(self.create_class(
                module,
                context,
                self.module.class_decls.get(*id).unwrap(),
            )?),
            TypeKind::ConstArray(len) => AnyTypeEnum::ArrayType(
                match self.convert_type(module, context, &typedef.generic_args[0])? {
                    AnyTypeEnum::ArrayType(llvm_type) => llvm_type.array_type(*len as u32),
                    AnyTypeEnum::FloatType(llvm_type) => llvm_type.array_type(*len as u32),
                    AnyTypeEnum::IntType(llvm_type) => llvm_type.array_type(*len as u32),
                    AnyTypeEnum::PointerType(llvm_type) => llvm_type.array_type(*len as u32),
                    AnyTypeEnum::StructType(llvm_type) => llvm_type.array_type(*len as u32),
                    _ => return Err(CodegenError::new("failed getting array of invalid type")),
                },
            ),
            TypeKind::Ptr => AnyTypeEnum::PointerType(
                match self.convert_type(module, context, &typedef.generic_args[0])? {
                    AnyTypeEnum::ArrayType(llvm_type) => llvm_type.ptr_type(AddressSpace::Generic),
                    AnyTypeEnum::FloatType(llvm_type) => llvm_type.ptr_type(AddressSpace::Generic),
                    AnyTypeEnum::IntType(llvm_type) => llvm_type.ptr_type(AddressSpace::Generic),
                    AnyTypeEnum::PointerType(llvm_type) => {
                        llvm_type.ptr_type(AddressSpace::Generic)
                    }
                    AnyTypeEnum::StructType(llvm_type) => llvm_type.ptr_type(AddressSpace::Generic),
                    _ => return Err(CodegenError::new("failed getting pointer of invalid type")),
                },
            ),
        })
    }

    fn eval_const<'c>(
        &self,
        context: &'c Context,
        module: &LlvmModule<'c>,
        builder: &Builder<'c>,
        vars: &HashMap<usize, PointerValue<'c>>,
        block: &Block,
        val: &Const,
    ) -> Result<BasicValueEnum<'c>> {
        Ok(match val {
            Const::Bool(b) => {
                BasicValueEnum::IntValue(context.bool_type().const_int(*b as u64, false))
            }
            Const::Int(i) => {
                BasicValueEnum::IntValue(context.i32_type().const_int(*i as u64, true))
            }
            Const::Int64(i) => {
                BasicValueEnum::IntValue(context.i64_type().const_int(*i as u64, true))
            }
            Const::Float(f) => {
                BasicValueEnum::FloatValue(context.f32_type().const_float(*f as f64))
            }
            Const::Array((t, a)) => {
                let mut items = vec![];

                for item in a.iter() {
                    items.push(self.eval(context, module, builder, vars, block, item)?);
                }

                BasicValueEnum::ArrayValue(match self.convert_type(module, context, &t)? {
                    AnyTypeEnum::ArrayType(llvm_type) => llvm_type.const_array(
                        items
                            .iter()
                            .map(|i| i.into_array_value())
                            .collect::<Vec<_>>()
                            .as_slice(),
                    ),
                    AnyTypeEnum::IntType(llvm_type) => llvm_type.const_array(
                        items
                            .iter()
                            .map(|i| i.into_int_value())
                            .collect::<Vec<_>>()
                            .as_slice(),
                    ),
                    AnyTypeEnum::FloatType(llvm_type) => llvm_type.const_array(
                        items
                            .iter()
                            .map(|i| i.into_float_value())
                            .collect::<Vec<_>>()
                            .as_slice(),
                    ),
                    AnyTypeEnum::StructType(llvm_type) => llvm_type.const_array(
                        items
                            .iter()
                            .map(|i| i.into_struct_value())
                            .collect::<Vec<_>>()
                            .as_slice(),
                    ),
                    AnyTypeEnum::PointerType(llvm_type) => llvm_type.const_array(
                        items
                            .iter()
                            .map(|i| i.into_pointer_value())
                            .collect::<Vec<_>>()
                            .as_slice(),
                    ),
                    _ => return Err(CodegenError::new("invalid type for const array")),
                })
            }
            Const::Float64(f) => BasicValueEnum::FloatValue(context.f64_type().const_float(*f)),
        })
    }

    fn none<'c>(&self, context: &'c Context) -> BasicValueEnum<'c> {
        context.bool_type().const_int(1, false).into()
    }

    fn eval<'c>(
        &self,
        context: &'c Context,
        module: &LlvmModule<'c>,
        builder: &Builder<'c>,
        vars: &HashMap<usize, PointerValue<'c>>,
        block: &Block,
        ir: &IR,
    ) -> Result<BasicValueEnum<'c>> {
        match ir {
            IR::LoadConst(val) => self.eval_const(context, module, builder, vars, block, val),
            IR::LoadFnParam(id) => {
                let llvm_block = builder.get_insert_block().unwrap();
                let fn_value = llvm_block.get_parent().unwrap();
                let value = fn_value.get_nth_param(*id as u32).unwrap();

                Ok(value)
            }
            IR::LoadArrayItem((ir, index)) => {
                let agg = self
                    .eval(context, module, builder, vars, block, &ir)?
                    .into_array_value();

                Ok(builder
                    .build_extract_value(agg, *index as u32, "load_array_item")
                    .unwrap())
            }
            IR::Load(ir) => {
                let value = self
                    .eval(context, module, builder, vars, block, &ir)?
                    .into_pointer_value();

                Ok(builder.build_load(value, "load"))
            }
            IR::GetName(id) => {
                let var = vars.get(id).unwrap();

                Ok((*var).into())
            }
            IR::GetProperty((ir, field_id)) => {
                let ptr = self
                    .eval(context, module, builder, vars, block, &ir)?
                    .into_pointer_value();
                let class_name = ptr.get_name().to_str().unwrap();
                let property_ptr = builder
                    .build_struct_gep(
                        ptr,
                        *field_id as u32,
                        &format!("prop_{}_f{}", class_name, field_id),
                    )
                    .unwrap();

                Ok(property_ptr.into())
            }
            IR::LoadByteArray(id) => {
                let byte_array = block.byte_arrays.get(*id).unwrap();
                let ptr = builder.build_alloca(
                    context.i8_type().array_type(byte_array.data.len() as u32),
                    "byte_array_alloca",
                );

                let const_array = context.i8_type().const_array(
                    byte_array
                        .data
                        .iter()
                        .map(|b| context.i8_type().const_int(*b as u64, false))
                        .collect::<Vec<_>>()
                        .as_slice(),
                );

                builder.build_store(ptr, const_array);

                let ptr = builder.build_pointer_cast(
                    ptr,
                    context.i8_type().ptr_type(AddressSpace::Generic),
                    "cast_byte_array_ptr",
                );

                Ok(BasicValueEnum::PointerValue(ptr))
            }
            IR::StoreName((id, inner_ir)) => {
                let var = vars.get(id).unwrap();
                let value = self.eval(context, module, builder, vars, block, &inner_ir)?;

                builder.build_store(*var, value);

                Ok(value)
            }
            IR::Store((key, value)) => {
                let key = self
                    .eval(context, module, builder, vars, block, &key)?
                    .into_pointer_value();
                let value = self.eval(context, module, builder, vars, block, &value)?;

                builder.build_store(key, value);

                Ok(value)
            }
            IR::Cast((inner_ir, dest_type)) => {
                let llvm_type = self.convert_basic_type(module, context, &dest_type)?;
                let value = self.eval(context, module, builder, vars, block, &inner_ir)?;

                Ok(match llvm_type {
                    BasicTypeEnum::IntType(int_type) => builder
                        .build_int_cast(
                            value.into_int_value(),
                            int_type,
                            &format!("cast_to_{}", dest_type.name),
                        )
                        .into(),
                    BasicTypeEnum::FloatType(float_type) => builder
                        .build_float_cast(
                            value.into_float_value(),
                            float_type,
                            &format!("cast_to_{}", dest_type.name),
                        )
                        .into(),
                    _ => {
                        return Err(CodegenError::new(format!(
                            "unable to cast to {}",
                            dest_type.name
                        )))
                    }
                })
            }
            IR::Call((fn_name, args)) => {
                let mut call_args = vec![];

                for arg in args.iter() {
                    let value = self.eval(context, module, builder, vars, block, &arg)?;

                    call_args.push(value);
                }

                let func = module.get_function(&fn_name).unwrap();
                let return_value =
                    builder.build_call(func, call_args.as_slice(), &format!("call_{}", fn_name));

                if let Some(left) = return_value.try_as_basic_value().left() {
                    return Ok(left);
                }

                // @TODO: how to support void here?
                Ok(self.none(context))
            }
            IR::IntAdd((left, right)) => Ok(builder
                .build_int_add(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_add",
                )
                .into()),
            IR::IntSub((left, right)) => Ok(builder
                .build_int_sub(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_sub",
                )
                .into()),
            IR::IntMul((left, right)) => Ok(builder
                .build_int_mul(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_mul",
                )
                .into()),
            IR::IntSignDiv((left, right)) => Ok(builder
                .build_int_signed_div(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_signed_div",
                )
                .into()),
            IR::IntUnsignDiv((left, right)) => Ok(builder
                .build_int_unsigned_div(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_unsigned_div",
                )
                .into()),
            IR::IntCmpLt((left, right)) => Ok(builder
                .build_int_compare(
                    IntPredicate::SLT,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_less_than",
                )
                .into()),
            IR::IntCmpLte((left, right)) => Ok(builder
                .build_int_compare(
                    IntPredicate::SLE,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_less_than_eq",
                )
                .into()),
            IR::IntCmpGt((left, right)) => Ok(builder
                .build_int_compare(
                    IntPredicate::SGT,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_greater_than",
                )
                .into()),
            IR::IntCmpGte((left, right)) => Ok(builder
                .build_int_compare(
                    IntPredicate::SGE,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_greater_than_eq",
                )
                .into()),
            IR::IntCmpEq((left, right)) => Ok(builder
                .build_int_compare(
                    IntPredicate::EQ,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_eq",
                )
                .into()),
            IR::IntCmpNeq((left, right)) => Ok(builder
                .build_int_compare(
                    IntPredicate::NE,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_int_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_int_value(),
                    "int_neq",
                )
                .into()),
            IR::FloatAdd((left, right)) => Ok(builder
                .build_float_add(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_add",
                )
                .into()),
            IR::FloatSub((left, right)) => Ok(builder
                .build_float_sub(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_sub",
                )
                .into()),
            IR::FloatMul((left, right)) => Ok(builder
                .build_float_mul(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_mul",
                )
                .into()),
            IR::FloatDiv((left, right)) => Ok(builder
                .build_float_div(
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_div",
                )
                .into()),
            IR::FloatCmpLt((left, right)) => Ok(builder
                .build_float_compare(
                    FloatPredicate::OLT,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_less_than",
                )
                .into()),
            IR::FloatCmpLte((left, right)) => Ok(builder
                .build_float_compare(
                    FloatPredicate::OLE,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_less_than_eq",
                )
                .into()),
            IR::FloatCmpGt((left, right)) => Ok(builder
                .build_float_compare(
                    FloatPredicate::OGT,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_greater_than",
                )
                .into()),
            IR::FloatCmpGte((left, right)) => Ok(builder
                .build_float_compare(
                    FloatPredicate::OGE,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_greater_than_eq",
                )
                .into()),
            IR::FloatCmpEq((left, right)) => Ok(builder
                .build_float_compare(
                    FloatPredicate::OEQ,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_eq",
                )
                .into()),
            IR::FloatCmpNeq((left, right)) => Ok(builder
                .build_float_compare(
                    FloatPredicate::ONE,
                    self.eval(context, module, builder, vars, block, &left)?
                        .into_float_value(),
                    self.eval(context, module, builder, vars, block, &right)?
                        .into_float_value(),
                    "float_neq",
                )
                .into()),
            IR::Init((id, props)) => {
                let mut values = vec![];

                for prop in props {
                    values.push(self.eval(context, module, builder, vars, block, &prop)?);
                }

                let class =
                    self.create_class(module, context, self.module.class_decls.get(*id).unwrap())?;
                let ptr = builder.build_alloca(class, &format!("new_c{}", id));

                for (i, value) in values.into_iter().enumerate() {
                    let property_ptr = builder
                        .build_struct_gep(ptr, i as u32, &format!("get_c{}_prop_{}", id, i))
                        .unwrap();

                    builder.build_store(property_ptr, value);
                }

                Ok(builder.build_load(ptr, &format!("load_c{}", id)))
            }
            IR::Cond((cond, if_block, else_block)) => {
                let comparison = self
                    .eval(context, module, builder, vars, block, &cond)?
                    .into_int_value();
                let current_block = builder.get_insert_block().unwrap();

                let if_llvm_block = context.insert_basic_block_after(current_block, &if_block.name);

                let if_builder = context.create_builder();
                if_builder.position_at_end(if_llvm_block);

                self.generate_block(context, module, &if_builder, &if_block, vars)?;

                let else_llvm_block = context.insert_basic_block_after(if_llvm_block, "else_block");

                let else_builder = context.create_builder();
                else_builder.position_at_end(else_llvm_block);

                if let Some(else_block) = else_block.as_ref() {
                    self.generate_block(context, module, &else_builder, &else_block, vars)?;
                }

                let end_block = context.insert_basic_block_after(else_llvm_block, "if_else_end");

                builder.build_conditional_branch(comparison, if_llvm_block, else_llvm_block);
                builder.position_at_end(end_block);

                if if_block.returns.is_none() {
                    if_builder.build_unconditional_branch(end_block);
                }

                match else_block.as_ref() {
                    None => {
                        else_builder.build_unconditional_branch(end_block);
                    }
                    Some(else_block) if else_block.returns.is_none() => {
                        else_builder.build_unconditional_branch(end_block);
                    }
                    _ => {}
                };

                Ok(self.none(context))
            }
            _ => Err(CodegenError::new("invalid IR")),
        }
    }

    fn generate_block<'c>(
        &self,
        context: &'c Context,
        module: &LlvmModule<'c>,
        builder: &Builder<'c>,
        block: &Block,
        vars: &HashMap<usize, PointerValue<'c>>,
    ) -> Result<()> {
        let mut vars = vars.clone();

        for var in block.var_decls.iter() {
            if !var.in_scope {
                continue;
            }

            let name = format!("v{}", var.id);

            vars.insert(
                var.id,
                builder.build_alloca(
                    self.convert_basic_type(module, context, &var.typedef)?,
                    &name,
                ),
            );
        }

        for ir in block.body.iter() {
            self.eval(context, module, builder, &vars, block, ir)?;
        }

        if let Some(returns) = &block.returns {
            let value = self.eval(context, module, builder, &vars, block, returns)?;

            builder.build_return(Some(&value));
        }

        Ok(())
    }

    fn generate_fn_decl<'c>(
        &self,
        context: &'c Context,
        module: &LlvmModule<'c>,
        fn_decl: &Func,
    ) -> Result<()> {
        let mut arg_types = vec![];

        for arg in fn_decl.args.iter() {
            arg_types.push(self.convert_basic_type(module, context, &arg.typedef)?);
        }

        let return_type = self.convert_type(module, context, &fn_decl.return_type)?;
        let fn_type = match return_type {
            AnyTypeEnum::FunctionType(llvm_type) => llvm_type,
            AnyTypeEnum::ArrayType(llvm_type) => llvm_type.fn_type(arg_types.as_slice(), false),
            AnyTypeEnum::FloatType(llvm_type) => llvm_type.fn_type(arg_types.as_slice(), false),
            AnyTypeEnum::IntType(llvm_type) => llvm_type.fn_type(arg_types.as_slice(), false),
            AnyTypeEnum::PointerType(llvm_type) => llvm_type.fn_type(arg_types.as_slice(), false),
            AnyTypeEnum::StructType(llvm_type) => llvm_type.fn_type(arg_types.as_slice(), false),
            AnyTypeEnum::VoidType(llvm_type) => llvm_type.fn_type(arg_types.as_slice(), false),
            _ => {
                return Err(CodegenError::new(format!(
                    "failed getting function-type for {}",
                    fn_decl.return_type.name
                )))
            }
        };

        let fn_value = module.add_function(
            fn_decl.name.as_str(),
            fn_type,
            if fn_decl.body.is_some() {
                None
            } else {
                Some(Linkage::External)
            },
        );

        if let Some(block) = &fn_decl.body {
            let basic_block = context.append_basic_block(fn_value, block.name.as_str());
            let builder = context.create_builder();

            builder.position_at_end(basic_block);

            self.generate_block(context, module, &builder, block, &HashMap::new())?;
        }

        Ok(())
    }

    pub fn generate(
        &self,
        out_file: impl AsRef<Path>,
        emit_ir: bool,
        optimize: bool,
    ) -> Result<()> {
        let context = Context::create();
        let module = context.create_module(&self.module.name);

        for fn_decl in self.module.func_decls.iter() {
            self.generate_fn_decl(&context, &module, fn_decl)?;
        }

        if emit_ir {
            println!("{}", module.print_to_string().to_string());
        }

        if let Err(e) = module.verify() {
            return Err(CodegenError::new(format!(
                "LLVM module is not valid: {}",
                e.to_string()
            )));
        }

        Target::initialize_x86(&InitializationConfig::default());

        let triple = TargetMachine::get_default_triple();
        let target = Target::from_triple(&triple).unwrap();
        let target_machine = target
            .create_target_machine(
                &triple,
                "x86-64",
                "+avx2",
                if optimize {
                    OptimizationLevel::Aggressive
                } else {
                    OptimizationLevel::None
                },
                RelocMode::Default,
                CodeModel::Default,
            )
            .unwrap();

        target_machine
            .write_to_file(&module, FileType::Object, out_file.as_ref())
            .map_err(|e| CodegenError::new(format!("failed to write to file: {}", e)))?;

        Ok(())
    }
}
