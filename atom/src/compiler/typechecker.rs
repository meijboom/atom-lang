use super::ir::{Func, Type};
use super::result::{CompileError, Result};

static SAFE_CASTS: &[(&str, &str)] = &[("int", "int64"), ("float", "float64")];

pub enum TypeResolution {
    Match,
    CastTo(Type),
    CastFrom(Type),
}

pub struct Typechecker {}

impl Typechecker {
    pub fn render_signature(func: &Func, arg_types: &Vec<Type>) -> String {
        let mut signature = String::from(func.name.as_str());

        signature.push('(');

        for (i, arg_type) in arg_types.iter().enumerate() {
            signature.push_str(&format!("{}", arg_type));

            if i < arg_types.len() - 1 {
                signature.push(',');
                signature.push(' ');
            }
        }

        signature.push(')');

        signature
    }

    pub fn strict_check(pos: (usize, usize), left: &Type, right: &Type) -> Result<()> {
        match Self::check(pos, left, right)? {
            TypeResolution::Match => Ok(()),
            _ => Err(Self::type_error(pos, left, right)),
        }
    }

    pub fn check(pos: (usize, usize), left: &Type, right: &Type) -> Result<TypeResolution> {
        if left.name.as_str() != right.name.as_str() {
            if SAFE_CASTS
                .iter()
                .any(|(l, r)| l == &left.name && r == &right.name)
            {
                return Ok(TypeResolution::CastTo(right.clone()));
            }

            if SAFE_CASTS
                .iter()
                .any(|(l, r)| r == &left.name && l == &right.name)
            {
                return Ok(TypeResolution::CastFrom(left.clone()));
            }

            return Err(Self::type_error(pos, left, right));
        }

        if left.generic_args.len() != right.generic_args.len() {
            return Err(Self::type_error(pos, left, right));
        }

        for i in 0..left.generic_args.len() {
            Self::strict_check(pos, &left.generic_args[i], &right.generic_args[i])?;
        }

        Ok(TypeResolution::Match)
    }

    pub fn type_error(pos: (usize, usize), left: &Type, right: &Type) -> CompileError {
        CompileError::new(format!("invalid type {} != {}", left, right,), pos)
    }

    pub fn arg_count_error(pos: (usize, usize), func: &Func, args: &Vec<Type>) -> CompileError {
        CompileError::new(
            format!(
                "no such function {} (invalid argument count)",
                Self::render_signature(func, args)
            ),
            pos,
        )
    }
}
