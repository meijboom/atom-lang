mod compiler;
pub mod ir;
mod result;
mod scope;
mod symbol_table;
mod typechecker;

pub use compiler::Compiler;
pub use typechecker::Typechecker;
