use std::collections::HashMap;

use super::ir::{Class, Func, Type, Var};

#[derive(Clone)]
pub enum Symbol {
    Var(Var),
    Type(Type),
    Class(Class),
    Function(Func),
}

pub struct SymbolTable {
    table: HashMap<String, Symbol>,
}

impl SymbolTable {
    pub fn new() -> Self {
        Self {
            table: HashMap::new(),
        }
    }

    pub fn lookup(&self, name: &str) -> Option<&Symbol> {
        self.table.get(name)
    }

    pub fn add(&mut self, name: impl AsRef<str>, symbol: Symbol) {
        self.table.insert(name.as_ref().to_string(), symbol);
    }
}
