use std::fmt;

use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize)]
pub struct Property {
    pub name: String,
    pub typedef: Type,
    pub is_public: bool,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Class {
    pub id: usize,
    pub name: String,
    pub props: Vec<Property>,
}

impl Class {
    pub fn get_prop_index(&self, name: impl AsRef<str>) -> Option<usize> {
        self.props
            .iter()
            .enumerate()
            .filter(|(_, p)| p.name == name.as_ref())
            .map(|(i, _)| i)
            .next()
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub enum TypeKind {
    Ptr,
    Void,
    Bool,
    Float,
    Float64,
    Unknown,
    Int,
    Int8,
    Int64,
    Class(usize),
    ConstArray(usize),
}

#[derive(Clone, Serialize, Deserialize)]
pub struct PropertyInfo {
    pub class: Class,
    pub property: Property,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Type {
    pub name: String,
    pub kind: TypeKind,
    pub alias: Option<String>,
    pub generic_args: Vec<Type>,
    pub property_info: Option<Box<PropertyInfo>>,
}

impl Type {
    pub fn new(name: String, kind: TypeKind) -> Self {
        Self {
            name,
            kind,
            alias: None,
            property_info: None,
            generic_args: vec![],
        }
    }

    pub fn new_with_args(name: String, kind: TypeKind, generic_args: Vec<Type>) -> Self {
        Self {
            name,
            kind,
            alias: None,
            generic_args,
            property_info: None,
        }
    }

    pub fn as_ident(&self) -> String {
        if let Some(alias) = &self.alias {
            return alias.clone();
        }

        format!("{}", self).replace("<", "__").replace(">", "")
    }
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(alias) = &self.alias {
            return f.write_str(alias);
        }

        f.write_str(&self.name)?;

        if !self.generic_args.is_empty() {
            f.write_str("<")?;

            for (i, arg) in self.generic_args.iter().enumerate() {
                arg.fmt(f)?;

                if i < self.generic_args.len() - 1 {
                    f.write_str(", ")?;
                }
            }

            f.write_str(">")?;
        }

        Ok(())
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Var {
    pub id: usize,
    pub name: String,
    pub typedef: Type,
    pub mutable: bool,
    pub in_scope: bool,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct FuncArg {
    pub typedef: Type,
    pub mutable: bool,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Func {
    pub name: String,
    pub class: Option<usize>,
    pub args: Vec<FuncArg>,
    pub return_type: Type,
    pub body: Option<Block>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ByteArray {
    pub id: usize,
    pub data: Vec<u8>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Block {
    pub name: String,
    pub var_decls: Vec<Var>,
    pub body: Vec<IR>,
    pub returns: Option<IR>,
    pub byte_arrays: Vec<ByteArray>,
}

impl Block {
    pub fn new(name: impl ToString) -> Self {
        Self {
            name: name.to_string(),
            var_decls: vec![],
            body: vec![],
            returns: None,
            byte_arrays: vec![],
        }
    }

    pub fn new_child(parent: &Block, name: impl ToString) -> Self {
        Self {
            name: name.to_string(),
            var_decls: parent
                .var_decls
                .iter()
                .map(|v| {
                    let mut v = v.clone();

                    v.in_scope = false;
                    v
                })
                .collect::<Vec<_>>(),
            body: vec![],
            returns: None,
            byte_arrays: vec![],
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
pub enum Const {
    Bool(bool),
    Int(i32),
    Int64(i64),
    Float(f32),
    Float64(f64),
    Array((Type, Vec<IR>)),
}

#[derive(Clone, Serialize, Deserialize)]
pub enum IR {
    Load(Box<IR>),
    LoadConst(Const),
    LoadStruct(usize),
    LoadByteArray(usize),
    LoadFnParam(usize),
    LoadArrayItem((Box<IR>, usize)),
    GetName(usize),
    GetProperty((Box<IR>, usize)),
    Cast((Box<IR>, Type)),
    Call((String, Vec<IR>)),
    Init((usize, Vec<IR>)),
    Cond((Box<IR>, Box<Block>, Box<Option<Block>>)),
    Store((Box<IR>, Box<IR>)),
    StoreName((usize, Box<IR>)),
    IntAdd((Box<IR>, Box<IR>)),
    IntSub((Box<IR>, Box<IR>)),
    IntMul((Box<IR>, Box<IR>)),
    IntSignDiv((Box<IR>, Box<IR>)),
    IntUnsignDiv((Box<IR>, Box<IR>)),
    IntCmpLt((Box<IR>, Box<IR>)),
    IntCmpLte((Box<IR>, Box<IR>)),
    IntCmpGt((Box<IR>, Box<IR>)),
    IntCmpGte((Box<IR>, Box<IR>)),
    IntCmpEq((Box<IR>, Box<IR>)),
    IntCmpNeq((Box<IR>, Box<IR>)),
    FloatAdd((Box<IR>, Box<IR>)),
    FloatSub((Box<IR>, Box<IR>)),
    FloatMul((Box<IR>, Box<IR>)),
    FloatDiv((Box<IR>, Box<IR>)),
    FloatCmp((Box<IR>, Box<IR>)),
    FloatCmpLt((Box<IR>, Box<IR>)),
    FloatCmpLte((Box<IR>, Box<IR>)),
    FloatCmpGt((Box<IR>, Box<IR>)),
    FloatCmpGte((Box<IR>, Box<IR>)),
    FloatCmpEq((Box<IR>, Box<IR>)),
    FloatCmpNeq((Box<IR>, Box<IR>)),
}

impl IR {
    pub fn unpack_load(self) -> Self {
        match self {
            Self::Load(ir) => *ir,
            ir => ir,
        }
    }

    pub fn is_const(&self) -> bool {
        match self {
            IR::Load(ir) => ir.is_const(),
            IR::LoadConst(_) => true,
            IR::LoadStruct(_) => true,
            IR::LoadByteArray(_) => true,
            IR::LoadArrayItem(_) => true,
            IR::LoadFnParam(_) => false,
            IR::GetName(_) => true,
            IR::GetProperty((ir, _)) => ir.is_const(),
            IR::Cast(_) => false,
            IR::Call(_) => false,
            IR::Cond(_) => false,
            IR::Init(_) => false,
            IR::Store((k, v)) => k.is_const() && v.is_const(),
            IR::StoreName(_) => false,
            IR::IntAdd((left, right)) => left.is_const() && right.is_const(),
            IR::IntSub((left, right)) => left.is_const() && right.is_const(),
            IR::IntMul((left, right)) => left.is_const() && right.is_const(),
            IR::IntSignDiv((left, right)) => left.is_const() && right.is_const(),
            IR::IntUnsignDiv((left, right)) => left.is_const() && right.is_const(),
            IR::IntCmpLt((left, right)) => left.is_const() && right.is_const(),
            IR::IntCmpLte((left, right)) => left.is_const() && right.is_const(),
            IR::IntCmpGt((left, right)) => left.is_const() && right.is_const(),
            IR::IntCmpGte((left, right)) => left.is_const() && right.is_const(),
            IR::IntCmpEq((left, right)) => left.is_const() && right.is_const(),
            IR::IntCmpNeq((left, right)) => left.is_const() && right.is_const(),
            IR::FloatAdd((left, right)) => left.is_const() && right.is_const(),
            IR::FloatSub((left, right)) => left.is_const() && right.is_const(),
            IR::FloatMul((left, right)) => left.is_const() && right.is_const(),
            IR::FloatDiv((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmp((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmpLt((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmpLte((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmpGt((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmpGte((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmpEq((left, right)) => left.is_const() && right.is_const(),
            IR::FloatCmpNeq((left, right)) => left.is_const() && right.is_const(),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Module {
    pub name: String,
    pub body: Vec<IR>,
    pub func_decls: Vec<Func>,
    pub class_decls: Vec<Class>,
}
