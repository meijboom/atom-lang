use logos::Logos;

use super::ir::{
    Block, ByteArray, Class, Const, Func, FuncArg, Module, Property, PropertyInfo, Type, TypeKind,
    Var, IR,
};
use super::result::{CompileError, Result};
use super::scope::{ScopeKind, ScopeTree};
use super::symbol_table::Symbol;
use super::typechecker::{TypeResolution, Typechecker};
use crate::syntax::{ast, Parser, Token};

static BUILTIN_TYPES: &[(&str, TypeKind)] = &[
    ("bool", TypeKind::Bool),
    ("int", TypeKind::Int),
    ("int64", TypeKind::Int64),
    ("float", TypeKind::Float),
    ("float64", TypeKind::Float64),
    ("byte", TypeKind::Int8),
    ("void", TypeKind::Void),
];

pub struct Value {
    pub typedef: Type,
    pub ir: IR,
}

impl Value {
    pub fn new(typedef: Type, ir: IR) -> Self {
        Self { typedef, ir }
    }
}

fn get_ident_name(expr: &ast::Node<ast::Expr>) -> Result<String> {
    match &expr.kind {
        ast::Expr::Ident(name) => Ok(name.clone()),
        _ => Err(CompileError::new(
            format!(
                "{} not supported (only identifier is supported for get_ident_name)",
                expr.kind.name(),
            ),
            expr.pos_start,
        )),
    }
}

pub struct Compiler {
    scope: ScopeTree,
    pos: ast::Pos,
    stack: Vec<Value>,
}

impl Compiler {
    pub fn new() -> Self {
        Self {
            scope: ScopeTree::new(),
            pos: (0, 0),
            stack: vec![],
        }
    }

    fn pop_stack(&mut self) -> Result<Value> {
        let value = self.stack.pop().expect("assert: stack is empty");

        Ok(value)
    }

    fn make_type(&mut self, typedef: &ast::Type) -> Result<Type> {
        let base_type = match self.scope.lookup_symbol(&typedef.name, None) {
            Some(Symbol::Type(t)) => t.clone(),
            Some(Symbol::Class(class)) => {
                Type::new(typedef.name.clone(), TypeKind::Class(class.id))
            }
            _ => {
                return Err(CompileError::new(
                    format!("no such type: {}", typedef.name),
                    self.pos,
                ))
            }
        };

        if !typedef.generic_args.is_empty() {
            panic!("assert: generic args not supported");
        }

        Ok(base_type)
    }

    fn visit_pair(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        left: &ast::Node<ast::Expr>,
        right: &ast::Node<ast::Expr>,
    ) -> Result<(Value, Value)> {
        self.visit_expr(module, block, left)?;
        self.visit_expr(module, block, right)?;

        let mut right = self.pop_stack()?;
        let mut left = self.pop_stack()?;

        match Typechecker::check(self.pos, &left.typedef, &right.typedef) {
            Ok(TypeResolution::CastTo(dest)) => {
                left = Value::new(dest.clone(), IR::Cast((Box::new(left.ir), dest)));
            }
            Ok(TypeResolution::CastFrom(src)) => {
                right = Value::new(src.clone(), IR::Cast((Box::new(right.ir), src)));
            }
            Ok(TypeResolution::Match) => {}
            Err(e) => return Err(e),
        };

        Ok((left, right))
    }

    fn visit_arithmetic_op(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        op: &ast::ArithmeticOp,
        left: &ast::Node<ast::Expr>,
        right: &ast::Node<ast::Expr>,
    ) -> Result<()> {
        let (left, right) = self.visit_pair(module, block, left, right)?;
        let value = match left.typedef.kind {
            TypeKind::Int | TypeKind::Int8 | TypeKind::Int64 => Value::new(
                left.typedef.clone(),
                match op {
                    ast::ArithmeticOp::Add => IR::IntAdd((left.ir.into(), right.ir.into())),
                    ast::ArithmeticOp::Sub => IR::IntSub((left.ir.into(), right.ir.into())),
                    ast::ArithmeticOp::Mul => IR::IntMul((left.ir.into(), right.ir.into())),
                    ast::ArithmeticOp::Div => IR::IntSignDiv((left.ir.into(), right.ir.into())),
                },
            ),
            TypeKind::Float | TypeKind::Float64 => Value::new(
                left.typedef.clone(),
                match op {
                    ast::ArithmeticOp::Add => IR::FloatAdd((left.ir.into(), right.ir.into())),
                    ast::ArithmeticOp::Sub => IR::FloatSub((left.ir.into(), right.ir.into())),
                    ast::ArithmeticOp::Mul => IR::FloatMul((left.ir.into(), right.ir.into())),
                    ast::ArithmeticOp::Div => IR::FloatDiv((left.ir.into(), right.ir.into())),
                },
            ),
            _ => {
                return Err(CompileError::new(
                    format!("unable to {} {}", op.name(), left.typedef),
                    self.pos,
                ))
            }
        };

        self.stack.push(value);

        Ok(())
    }

    fn visit_binary_op(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        op: &ast::BinaryOp,
        left: &ast::Node<ast::Expr>,
        right: &ast::Node<ast::Expr>,
    ) -> Result<()> {
        let (left, right) = self.visit_pair(module, block, left, right)?;
        let ir = match left.typedef.kind {
            TypeKind::Int | TypeKind::Int8 | TypeKind::Int64 => match op {
                ast::BinaryOp::Lt => IR::IntCmpLt((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Lte => IR::IntCmpLte((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Gt => IR::IntCmpGt((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Gte => IR::IntCmpGte((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Eq => IR::IntCmpEq((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Neq => IR::IntCmpNeq((left.ir.into(), right.ir.into())),
            },
            TypeKind::Float | TypeKind::Float64 => match op {
                ast::BinaryOp::Lt => IR::FloatCmpLt((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Lte => IR::FloatCmpLte((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Gt => IR::FloatCmpGt((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Gte => IR::FloatCmpGte((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Eq => IR::FloatCmpEq((left.ir.into(), right.ir.into())),
                ast::BinaryOp::Neq => IR::FloatCmpNeq((left.ir.into(), right.ir.into())),
            },
            _ => {
                return Err(CompileError::new(
                    format!("unsupported binary operation for {}", left.typedef),
                    self.pos,
                ))
            }
        };

        self.stack.push(Value::new(
            Type::new("bool".to_string(), TypeKind::Bool),
            ir,
        ));

        Ok(())
    }

    fn visit_scalar(
        &mut self,
        module: &mut Module,
        _block: &mut Block,
        scalar: &ast::Scalar,
    ) -> Result<()> {
        let const_val = match scalar {
            ast::Scalar::Int(i) => Const::Int(*i),
            ast::Scalar::Int64(i) => Const::Int64(*i),
            ast::Scalar::Float(f) => Const::Float(*f),
            ast::Scalar::Float64(f) => Const::Float64(*f),
            ast::Scalar::Bool(b) => Const::Bool(*b),
            _ => panic!("assert: unsupported scalar"),
        };
        let typedef = match scalar {
            ast::Scalar::Int(_) => Type::new("int".to_string(), TypeKind::Int),
            ast::Scalar::Int64(_) => Type::new("int64".to_string(), TypeKind::Int64),
            ast::Scalar::Float(_) => Type::new("float".to_string(), TypeKind::Float),
            ast::Scalar::Float64(_) => Type::new("float64".to_string(), TypeKind::Float64),
            ast::Scalar::Bool(_) => Type::new("bool".to_string(), TypeKind::Bool),
            _ => panic!("assert: unsupported scalar"),
        };
        let value = Value::new(typedef, IR::LoadConst(const_val));

        self.stack.push(value);

        Ok(())
    }

    fn visit_ident(&mut self, module: &mut Module, block: &mut Block, name: &str) -> Result<()> {
        let var = self
            .scope
            .lookup_symbol(name, None)
            .and_then(|symbol| match symbol {
                Symbol::Var(var) => Some(var),
                _ => None,
            });

        match var {
            Some(var) => {
                self.stack.push(Value::new(
                    var.typedef.clone(),
                    IR::Load(IR::GetName(var.id).into()),
                ));

                Ok(())
            }
            None => Err(CompileError::new(
                format!("unknown variable {}", name),
                self.pos,
            )),
        }
    }

    fn visit_fn_call(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        callee: &ast::Node<ast::Expr>,
        args: &Vec<ast::Node<ast::Expr>>,
    ) -> Result<()> {
        let name = get_ident_name(callee)?;
        let func = match self.scope.lookup_symbol(&name, None) {
            Some(Symbol::Function(func)) => func,
            _ => {
                return Err(CompileError::new(
                    format!("unknown function {}", name),
                    self.pos,
                ))
            }
        };

        if func.args.len() != args.len() {
            return Err(CompileError::new(
                format!(
                    "invalid argument count for {}() ({} != {})",
                    func.name,
                    args.len(),
                    func.args.len(),
                ),
                self.pos,
            ));
        }

        let mut call_args = vec![];

        for (i, arg) in args.iter().enumerate() {
            self.visit_expr(module, block, arg)?;

            let value = self.pop_stack()?;

            call_args.push(
                match Typechecker::check(
                    self.pos,
                    &func.args.get(i).unwrap().typedef,
                    &value.typedef,
                ) {
                    Ok(resolution) => match resolution {
                        TypeResolution::CastTo(dest) => IR::Cast((value.ir.into(), dest)),
                        TypeResolution::CastFrom(src) => IR::Cast((value.ir.into(), src)),
                        TypeResolution::Match => value.ir,
                    },
                    Err(e) => return Err(e),
                },
            );
        }

        self.stack.push(Value::new(
            func.return_type.clone(),
            IR::Call((name, call_args)),
        ));

        Ok(())
    }

    fn visit_member(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        object: &ast::Node<ast::Expr>,
        prop: &ast::Node<ast::Ident>,
    ) -> Result<()> {
        self.visit_expr(module, block, object)?;

        let object = self.pop_stack()?;
        let class = match self.scope.lookup_symbol(&object.typedef.name, None) {
            Some(Symbol::Class(class)) => class.clone(),
            _ => {
                return Err(CompileError::new(
                    format!("unknown class {}", object.typedef.name),
                    self.pos,
                ))
            }
        };
        let prop_index = match class.get_prop_index(&prop.kind.name) {
            Some(index) => index,
            None => {
                return Err(CompileError::new(
                    format!(
                        "unknown property {}.{}",
                        object.typedef.name, prop.kind.name
                    ),
                    self.pos,
                ))
            }
        };
        let property = class.props.get(prop_index).unwrap();
        let mut prop_type = property.typedef.clone();

        prop_type.property_info = Some(
            PropertyInfo {
                class: class.clone(),
                property: property.clone(),
            }
            .into(),
        );

        self.stack.push(Value::new(
            prop_type,
            IR::Load(IR::GetProperty((object.ir.unpack_load().into(), prop_index)).into()),
        ));

        Ok(())
    }

    fn visit_expr(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        node: &ast::Node<ast::Expr>,
    ) -> Result<()> {
        // @TODO: what should we do with this?
        //self.pos = node.pos_start;

        match &node.kind {
            ast::Expr::Scalar(scalar) => self.visit_scalar(module, block, &scalar)?,
            ast::Expr::Init((typedef, props)) => {
                self.visit_init(module, block, &typedef, &props)?
            }
            ast::Expr::ArithmeticOp((op, left, right)) => {
                self.visit_arithmetic_op(module, block, &op, left, right)?
            }
            ast::Expr::BinaryOp((op, left, right)) => {
                self.visit_binary_op(module, block, &op, left, right)?
            }
            ast::Expr::Ident(name) => self.visit_ident(module, block, &name)?,
            ast::Expr::Member((object, prop)) => {
                self.visit_member(module, block, &object, &prop)?
            }
            ast::Expr::FnCall((callee, args)) => {
                self.visit_fn_call(module, block, &callee, &args)?
            }
            kind => {
                return Err(CompileError::new(
                    format!("unexpected {}", kind.name()),
                    self.pos,
                ))
            }
        };

        Ok(())
    }

    fn visit_init(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        typedef: &ast::Type,
        props: &Vec<ast::InitProperty>,
    ) -> Result<()> {
        let class_type = self.make_type(typedef)?;
        let class =
            if let Some(Symbol::Class(class)) = self.scope.lookup_symbol(&class_type.name, None) {
                class.clone()
            } else {
                return Err(CompileError::new(
                    format!("class {} not found", class_type.name),
                    self.pos,
                ));
            };

        let mut values = Vec::with_capacity(props.len());

        for prop in props.iter() {
            if let Some(index) = class.get_prop_index(&prop.name) {
                let class_prop = class.props.get(index).unwrap();

                self.visit_expr(module, block, &prop.expr)?;

                let value = self.pop_stack()?;

                Typechecker::check(self.pos, &class_prop.typedef, &value.typedef).map_err(|e| {
                    CompileError::new(
                        format!(
                            "invalid property {}.{} ({})",
                            class_type.name, prop.name, e.message
                        ),
                        self.pos,
                    )
                })?;

                values.insert(index, value.ir);

                continue;
            }

            return Err(CompileError::new(
                format!("unknown property {}.{}", class_type, prop.name),
                self.pos,
            ));
        }

        self.stack
            .push(Value::new(class_type, IR::Init((class.id, values))));

        Ok(())
    }

    fn visit_assign(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        assign: &ast::Assign,
    ) -> Result<()> {
        self.visit_expr(module, block, &assign.value)?;

        let value = self.pop_stack()?;

        if let ast::Expr::Ident(name) = &assign.key.kind {
            let var = self
                .scope
                .lookup_symbol(&name, None)
                .and_then(|symbol| match symbol {
                    Symbol::Var(var) => Some(var),
                    _ => None,
                });
            let id = if assign.reassign {
                if var.is_none() {
                    return Err(CompileError::new(
                        format!("unknown variable {}", name),
                        self.pos,
                    ));
                }

                let var = var.unwrap();

                if !var.mutable {
                    return Err(CompileError::new(
                        format!("cannot assign twice to immutable variable {}", name),
                        self.pos,
                    ));
                }

                Typechecker::check(self.pos, &var.typedef, &value.typedef)?;

                var.id
            } else {
                if var.is_some() {
                    return Err(CompileError::new(
                        format!("variable {} already exists", name),
                        self.pos,
                    ));
                }

                let id = block.var_decls.len();
                let var = Var {
                    id,
                    in_scope: true,
                    name: name.to_string(),
                    typedef: value.typedef,
                    mutable: assign.mutable,
                };

                self.scope
                    .add_symbol(var.name.clone(), Symbol::Var(var.clone()), None);

                block.var_decls.push(var);

                id
            };

            block.body.push(IR::StoreName((id, value.ir.into())));
        } else {
            self.visit_expr(module, block, &assign.key)?;

            let object = self.pop_stack()?;

            match &object.typedef.property_info {
                Some(property_info) => {
                    if !property_info.property.is_public {
                        return Err(CompileError::new(
                            format!(
                                "property {}.{} is not public",
                                property_info.class.name, property_info.property.name,
                            ),
                            self.pos,
                        ));
                    }

                    block
                        .body
                        .push(IR::Store((object.ir.unpack_load().into(), value.ir.into())));
                }
                None => {
                    return Err(CompileError::new(
                        format!("unable to assign to {}", object.typedef),
                        self.pos,
                    ))
                }
            };
        }

        Ok(())
    }

    fn visit_if_else(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        if_else: &ast::IfElse,
    ) -> Result<()> {
        self.visit_expr(module, block, &if_else.cond)?;

        let value = self.pop_stack()?;

        Typechecker::check(
            self.pos,
            &Type::new("bool".to_string(), TypeKind::Bool),
            &value.typedef,
        )
        .map_err(|e| {
            CompileError::new(
                format!("unexpected {} for condition ({})", value.typedef, e.message),
                self.pos,
            )
        })?;

        let mut if_block = Block::new_child(block, "if_body".to_string());

        self.visit_tree(module, &mut if_block, &if_else.body)?;

        let else_block = match &if_else.or {
            Some(or) => {
                let mut or_block = Block::new_child(block, "else_if_body".to_string());
                let mut or_copy = or.clone();

                or_copy.alt = if_else.alt.to_vec();

                self.visit_if_else(module, &mut or_block, &or_copy)?;

                Some(or_block)
            }
            None => {
                if !if_else.alt.is_empty() {
                    let mut else_block = Block::new_child(block, "else_body".to_string());

                    self.visit_tree(module, &mut else_block, &if_else.alt)?;

                    Some(else_block)
                } else {
                    None
                }
            }
        };

        block.body.push(IR::Cond((
            value.ir.into(),
            if_block.into(),
            else_block.into(),
        )));

        Ok(())
    }

    fn visit_stmt(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        node: &ast::Node<ast::Stmt>,
    ) -> Result<()> {
        self.pos = node.pos_start;

        match &node.kind {
            ast::Stmt::Return(ret) => self.visit_return(module, block, &ret)?,
            ast::Stmt::Assign(assign) => self.visit_assign(module, block, &assign)?,
            ast::Stmt::If(if_else) => self.visit_if_else(module, block, &if_else)?,
            kind => {
                return Err(CompileError::new(
                    format!("unexpected {}", kind.name()),
                    self.pos,
                ))
            }
        };

        Ok(())
    }

    fn visit_root_stmt(&mut self, module: &mut Module, node: &ast::Node<ast::Stmt>) -> Result<()> {
        self.pos = node.pos_start;

        match &node.kind {
            ast::Stmt::FnDef(fn_def) => self.visit_fn_def(module, &fn_def)?,
            ast::Stmt::ClassDef(class_def) => self.visit_class_def(module, &class_def)?,
            kind => {
                return Err(CompileError::new(
                    format!("unexpected {}", kind.name()),
                    self.pos,
                ))
            }
        };

        Ok(())
    }

    fn visit_class_def(&mut self, module: &mut Module, class_def: &ast::ClassDef) -> Result<()> {
        let mut props = vec![];

        for prop in class_def.props.iter() {
            props.push(Property {
                name: prop.name.clone(),
                is_public: prop.is_public,
                typedef: self.make_type(&prop.typedef)?,
            });
        }

        let class = Class {
            id: module.class_decls.len(),
            name: class_def.name.clone(),
            props,
        };

        module.class_decls.push(class.clone());
        self.scope
            .add_symbol(class_def.name.clone(), Symbol::Class(class), None);

        Ok(())
    }

    fn visit_fn_def(&mut self, module: &mut Module, fn_def: &ast::FnDef) -> Result<()> {
        let typedef = self.make_type(&fn_def.return_type)?;
        let mut func = Func {
            name: fn_def.name.clone(),
            class: None,
            args: vec![],
            body: None,
            return_type: typedef.clone(),
        };

        for arg in fn_def.args.iter() {
            func.args.push(FuncArg {
                typedef: self.make_type(&arg.typedef)?,
                mutable: true,
            });
        }

        // register early to support recursion
        self.scope
            .add_symbol(&fn_def.name, Symbol::Function(func.clone()), None);

        let mut block = Block::new(&fn_def.name);

        self.scope.add_child(ScopeKind::Function(func.clone()));
        self.visit_tree(module, &mut block, &fn_def.body)?;
        self.scope.move_to_parent();

        func.body = Some(block);

        self.scope
            .add_symbol(&fn_def.name, Symbol::Function(func.clone()), None);

        module.func_decls.push(func);

        Ok(())
    }

    fn visit_return(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        ret: &ast::Node<ast::Expr>,
    ) -> Result<()> {
        self.visit_expr(module, block, ret)?;

        block.returns = Some(self.pop_stack()?.ir);

        Ok(())
    }

    fn visit_tree(
        &mut self,
        module: &mut Module,
        block: &mut Block,
        tree: &ast::Tree,
    ) -> Result<()> {
        for node in tree.iter() {
            self.visit_stmt(module, block, &node)?;
        }

        Ok(())
    }

    fn visit_root_tree(&mut self, module: &mut Module, tree: ast::Tree) -> Result<()> {
        for node in tree.iter() {
            self.visit_root_stmt(module, &node)?;
        }

        Ok(())
    }

    pub fn register_builtins(&mut self, module: &mut Module) {
        for (name, kind) in BUILTIN_TYPES {
            self.scope.add_symbol(
                name,
                Symbol::Type(Type::new(name.to_string(), kind.clone())),
                None,
            );
        }
    }

    pub fn compile(&mut self, tree: ast::Tree) -> Result<Module> {
        let mut module = Module {
            name: "main".to_string(),
            body: vec![],
            func_decls: vec![],
            class_decls: vec![],
        };

        self.register_builtins(&mut module);
        self.visit_root_tree(&mut module, tree)?;

        Ok(module)
    }
}
