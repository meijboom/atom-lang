use std::error;
use std::fmt;
use std::result;

#[derive(Debug)]
pub struct CompileError {
    pub pos: (usize, usize),
    pub message: String,
}

impl error::Error for CompileError {}

impl fmt::Display for CompileError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "CompileError: {} at {}:{}",
            self.message, self.pos.0, self.pos.1,
        )
    }
}

impl CompileError {
    pub fn new<T: ToString>(message: T, pos: (usize, usize)) -> Self {
        Self {
            pos,
            message: message.to_string(),
        }
    }
}

pub type Result<T> = result::Result<T, CompileError>;
