use super::ir::Func;
use super::symbol_table::{Symbol, SymbolTable};

pub enum ScopeKind {
    Global,
    Local,
    Function(Func),
}

pub struct Scope {
    pub id: usize,
    pub kind: ScopeKind,
    pub level: usize,
    pub parent: Option<usize>,

    symbol_table: SymbolTable,
}

impl Scope {
    pub fn new(id: usize, kind: ScopeKind, level: usize) -> Self {
        Self {
            id,
            kind,
            level,
            parent: None,
            symbol_table: SymbolTable::new(),
        }
    }

    pub fn new_with_parent(id: usize, kind: ScopeKind, level: usize, parent: usize) -> Self {
        Self {
            id,
            kind,
            level,
            parent: Some(parent),
            symbol_table: SymbolTable::new(),
        }
    }
}

pub struct ScopeTree {
    children: Vec<Scope>,
    current_id: usize,
}

impl ScopeTree {
    pub fn new() -> Self {
        Self {
            current_id: 0,
            children: vec![Scope::new(0, ScopeKind::Global, 1)],
        }
    }

    pub fn current(&self) -> &Scope {
        self.children
            .get(self.current_id)
            .expect("assert: invalid scope ID")
    }

    pub fn current_mut(&mut self) -> &mut Scope {
        self.children
            .get_mut(self.current_id)
            .expect("assert: invalid scope ID")
    }

    pub fn move_to_parent(&mut self) {
        self.current_id = self
            .children
            .get(self.current_id)
            .and_then(|s| s.parent)
            .expect("assert: unable to pop parent scope");
    }

    pub fn add_child(&mut self, kind: ScopeKind) {
        let level = self
            .children
            .get(self.current_id)
            .and_then(|s| Some(s.level))
            .unwrap();

        let scope = Scope::new_with_parent(self.children.len(), kind, level + 1, self.current_id);

        self.current_id = scope.id;
        self.children.push(scope);
    }

    pub fn add_symbol(&mut self, name: impl AsRef<str>, symbol: Symbol, scope_id: Option<usize>) {
        let id = scope_id.unwrap_or(self.current_id);
        let mut scope = self.children.get_mut(id).unwrap();

        scope.symbol_table.add(name, symbol);
    }

    pub fn lookup_symbol(&self, name: &str, scope_id: Option<usize>) -> Option<Symbol> {
        let id = scope_id.unwrap_or(self.current_id);
        let scope = self.children.get(id).unwrap();

        if let Some(symbol) = scope.symbol_table.lookup(name) {
            return Some(symbol.clone());
        }

        if let Some(parent) = scope.parent {
            return self.lookup_symbol(name, Some(parent));
        }

        None
    }
}
