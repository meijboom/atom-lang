use std::env::temp_dir;
use std::fs;
use std::path::PathBuf;
use std::process::{exit, Command};

use anyhow::{Context, Result};
use clap::Clap;
use logos::Logos;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use ron::ser::{to_string_pretty, PrettyConfig};

mod codegen;
mod compiler;
mod syntax;

use codegen::Codegen;
use compiler::Compiler;
use syntax::{Parser, Token};

#[derive(Clap)]
struct Opts {
    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Clap)]
struct BuildOpts {
    #[clap(short, long)]
    filename: PathBuf,
    #[clap(short, long, default_value = "out")]
    output_filename: PathBuf,
    #[clap(short, long, default_value = "libatom.a")]
    libatom_path: String,
    #[clap(long)]
    emit_atom_ir: bool,
    #[clap(long)]
    emit_llvm_ir: bool,
    #[clap(long)]
    show_ast: bool,
    #[clap(short, long)]
    release: bool,
}

#[derive(Clap)]
enum Cmd {
    #[clap(name = "build")]
    Build(BuildOpts),
    #[clap(name = "run")]
    Run(BuildOpts),
}

fn build(opts: BuildOpts) -> Result<()> {
    let source = fs::read_to_string(&opts.filename)
        .with_context(|| format!("unable to read input file {:?}", opts.filename))?;
    let parser = Parser::new(Token::lexer(&source));
    let tree = parser.parse()?;

    if opts.show_ast {
        println!("{}", to_string_pretty(&tree, PrettyConfig::new(),)?,);
    }

    let mut compiler = Compiler::new();
    let module = compiler.compile(tree)?;

    if opts.emit_atom_ir {
        println!("{}", to_string_pretty(&module, PrettyConfig::new(),)?,);
    }

    let llvm_out_filename = opts.output_filename.with_extension("o");
    let codegen = Codegen::new(module);
    codegen.generate(&llvm_out_filename, opts.emit_llvm_ir, opts.release)?;

    let mut opt_args = vec![];

    if opts.release {
        opt_args.push("-O2");
        opt_args.push("-flto");
        opt_args.push("-s");
    }

    Command::new("clang-10")
        .args(opt_args.as_slice())
        .arg("-g3")
        .arg("-ldl")
        .arg("-pthread")
        .arg(&llvm_out_filename)
        .arg(&opts.libatom_path)
        .arg("-o")
        .arg(opts.output_filename)
        .spawn()?
        .wait()?;

    Ok(())
}

fn run(mut opts: BuildOpts) -> Result<()> {
    let mut filename = temp_dir();
    let rand_string: String = thread_rng().sample_iter(&Alphanumeric).take(15).collect();

    filename.push(rand_string);

    opts.output_filename = filename.clone();

    build(opts)?;

    let status = Command::new("sh")
        .arg("-c")
        .arg(&filename)
        .spawn()?
        .wait()?;

    fs::remove_file(filename.with_extension("o"))?;
    fs::remove_file(filename)?;

    exit(status.code().unwrap_or(1));
}

fn main() {
    let opts = Opts::parse();
    let result = match opts.cmd {
        Cmd::Build(opts) => build(opts),
        Cmd::Run(opts) => run(opts),
    };

    match result {
        Ok(_) => {}
        Err(e) => {
            eprintln!("{:?}", e);
            exit(1);
        }
    };
}
