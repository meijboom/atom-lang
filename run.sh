#!/bin/bash

set -e

target="$1"

if [ "$target" = "" ]; then
    target="debug"
fi

cargo_flags=""

if [ "$target" = "release" ]; then
    cargo_flags="--release"
fi

mkdir -p target && \
    (cd libatom && cargo +nightly build ${cargo_flags}) && \
    (cd atom && cargo run ${cargo_flags} -- run -l ../libatom/target/${target}/liblibatom.a -f ../examples/main.atom)
