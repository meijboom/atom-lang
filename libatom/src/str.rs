use alloc::{boxed::Box, vec::Vec};
use core::{str, unicode::conversions};
use cstr_core::CString;
use libc::c_char;

#[repr(C)]
#[derive(Debug)]
pub struct Str {
    pub length: usize,
    pub capacity: usize,
    pub data: Vec<u8>,
}

unsafe fn bytes_ptr_to_vec(length: usize, data: *const u8) -> Vec<u8> {
    if data.is_null() {
        panic!("unable to create string when data is NULL");
    }

    let mut bytes: Vec<u8> = Vec::with_capacity(length);

    for i in 0..length {
        bytes.push(*data.offset(i as isize));
    }

    bytes
}

impl Str {
    pub fn new(length: usize, data: Vec<u8>) -> Self {
        Self::new_with_capacity(length, length, data)
    }

    pub fn new_with_capacity(length: usize, capacity: usize, data: Vec<u8>) -> Self {
        Self {
            length: length,
            capacity: capacity,
            data: data,
        }
    }

    // allocate null terminated C string from a Str pointer
    pub unsafe fn to_cstr(&self) -> *const c_char {
        CString::from_vec_unchecked(self.data.clone()).into_raw()
    }

    // turn this Str into a pointer to the Str
    pub unsafe fn into_ptr(self) -> *mut Str {
        Box::into_raw(Box::new(self))
    }
}

impl From<&str> for Str {
    fn from(s: &str) -> Self {
        Self::new(s.len(), s.as_bytes().to_vec())
    }
}

impl From<&[u8]> for Str {
    fn from(data: &[u8]) -> Self {
        Self::new(data.len(), data.to_vec())
    }
}

impl From<Vec<u8>> for Str {
    fn from(data: Vec<u8>) -> Self {
        Self::new(data.len(), data)
    }
}

#[no_mangle]
pub unsafe extern "C" fn str_upper(s: *mut Str) -> *mut Str {
    let s = &*s;
    let mut output: Vec<u8> = Vec::with_capacity(s.length as usize);

    for i in 0..s.data.len() {
        match conversions::to_upper(s.data[i] as char) {
            [a, '\0', _] => output.push(a as u8),
            [a, b, '\0'] => {
                output.push(a as u8);
                output.push(b as u8);
            }
            [a, b, c] => {
                output.push(a as u8);
                output.push(b as u8);
                output.push(c as u8);
            }
        }
    }

    Str::new(s.length, output).into_ptr()
}

#[no_mangle]
pub unsafe extern "C" fn str_lower(s: *mut Str) -> *mut Str {
    let s = &*s;
    let mut output: Vec<u8> = Vec::with_capacity(s.length as usize);

    for i in 0..s.data.len() {
        match conversions::to_lower(s.data[i] as char) {
            [a, '\0', _] => output.push(a as u8),
            [a, b, '\0'] => {
                output.push(a as u8);
                output.push(b as u8);
            }
            [a, b, c] => {
                output.push(a as u8);
                output.push(b as u8);
                output.push(c as u8);
            }
        }
    }

    Str::new(s.length, output).into_ptr()
}

#[no_mangle]
pub unsafe extern "C" fn str_concat(left: *mut Str, right: *mut Str) -> *mut Str {
    let left = &*left;
    let right = &*right;
    let slice = [&left.data[..], &right.data[..]].concat();

    Str::from(slice).into_ptr()
}

#[no_mangle]
pub unsafe extern "C" fn str_substr(s: *mut Str, start: usize) -> *mut Str {
    let s = &*s;

    Str::from(&s.data[start..]).into_ptr()
}

#[no_mangle]
pub unsafe extern "C" fn str_eq(left: *mut Str, right: *mut Str) -> bool {
    (&*left).data == (&*right).data
}

#[no_mangle]
pub unsafe extern "C" fn str_len(s: *mut Str) -> i64 {
    (*s).length as i64
}

#[no_mangle]
pub unsafe extern "C" fn new_str(length: usize, data: *const u8) -> *mut Str {
    Str::new(length, bytes_ptr_to_vec(length, data)).into_ptr()
}
