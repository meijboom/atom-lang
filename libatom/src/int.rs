use super::str::Str;

use itoa::Buffer;

#[no_mangle]
pub unsafe extern "C" fn int_to_str(val: i32) -> *mut Str {
    Str::from(Buffer::new().format(val)).into_ptr()
}

#[no_mangle]
pub unsafe extern "C" fn int64_to_str(val: i64) -> *mut Str {
    Str::from(Buffer::new().format(val)).into_ptr()
}
