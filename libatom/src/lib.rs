#![no_std]
#![feature(fn_traits)]
#![feature(lang_items)]
#![feature(str_internals)]
#![feature(unicode_internals)]
#![feature(vec_into_raw_parts)]
#![feature(alloc_error_handler)]

extern "C" {
    fn abort() -> !;
}

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

extern crate alloc;

use ::core::{alloc::Layout, panic::PanicInfo};

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    unsafe { abort() }
}

#[alloc_error_handler]
fn alloc_error(_: Layout) -> ! {
    unsafe { abort() }
}

#[lang = "eh_personality"]
extern "C" fn eh_personality() {}

mod core;
mod int;
mod str;
