use libc::printf;

use super::str::Str;

#[no_mangle]
pub unsafe extern "C" fn println(s: *mut Str) {
    let format = "%s\n\0";
    printf(format.as_ptr() as *const i8, (*s).to_cstr());
}
